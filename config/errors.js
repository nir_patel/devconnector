export const errors = {
    username_not_available: 'Username is not available',
    email_not_available: 'Email is not available',
    invalid_token: 'Invalid token',
    user_unauthorized: 'User not authorized',
    user_not_found: 'User not found',
    profile_not_found: 'Profile not found',
    experience_not_found: 'Experience not found',
    github_repo_not_found: 'Github profile not found',
    gitlab_repo_not_found: 'Gitlab profile not found',
    post_not_found: 'Post not found',
    login_failed: 'Username or password are incorrect',
    generic_error: 'Generic error',
    comment_not_found: 'Comment not found'
};
