import dotenv from 'dotenv';
dotenv.config();

const get = (key) => {
    return process.env[key];
};

export default { get };
