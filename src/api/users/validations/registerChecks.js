import { body } from 'express-validator';

const validations = [
    body('username', 'Username is required').notEmpty(),
    body('name', 'Name is required').notEmpty(),
    body('email', 'Email is invalid').isEmail(),
    body('password', 'Password must include at least 6 characters').isLength({
        min: 6
    })
];

export default validations;
