import { body } from 'express-validator';

const validations = [
    body('username', 'Username is required').not().isEmpty(),
    body('password', 'Password is required').not().isEmpty()
];

export default validations;