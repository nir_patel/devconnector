import logger from '../../../logger/logger.js';
import validations from '../validations/registerChecks.js';
import validateRequest from '../../../middlewares/validateRequest.js';
import createUser from '../../../services/createUser.js';
import { resErrors, success } from '../../../utils/parseMessage.js';
import signToken from '../../../utils/signToken.js';
import { StatusCodes } from 'http-status-codes';

// @route   POST api/users/register
// @desc    Register user
// @access  Public
const registerRoute = router =>
    router.post('/register', validations, validateRequest, async (req, res) => {
        logger.log(`Received new users/register request: ${JSON.stringify(req.body)}`);

        let result;

        try {
            result = await createUser(req.body);
        } catch (err) {
            logger.error(
                `User creation of ${req.body.username} failed with an error: ${err}`
            );
            return res
                .status(StatusCodes.INTERNAL_SERVER_ERROR)
                .json(resErrors([err.message ?? err]));
        }

        if (result.error) {
            logger.error(`Unable to create user ${req.body.username}: ${result.error}`);
            return res.status(StatusCodes.BAD_REQUEST).json(resErrors([result.error]));
        }

        logger.info(`User ${req.body.username} created successfully`);

        const payload = extractData(result);

        signToken(payload, (err, token) => {
            if (err) {
                return res
                    .status(StatusCodes.INTERNAL_SERVER_ERROR)
                    .json(resErrors([err]));
            }
            return res
                .status(StatusCodes.CREATED)
                .json(success({ token, data: payload }));
        });
    });

const extractData = ({ _id, username, name, email, avatar, date }) => {
    return { _id, username, name, email, avatar, date };
};

export default registerRoute;
