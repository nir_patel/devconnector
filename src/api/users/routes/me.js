import { StatusCodes } from 'http-status-codes';
import { errors } from '../../../../config/errors.js';
import logger from '../../../logger/logger.js';
import auth from '../../../middlewares/auth.js';
import getUserById from '../../../services/getUserById.js';
import { resErrors, success } from '../../../utils/parseMessage.js';

// @route   GET api/users/me
// @desc    Get user's own data
// @access  Private
const me = router => {
    router.get('/me', auth, async (req, res) => {
        try {
            const user = await getUserById(req.user._id);

            res.status(StatusCodes.OK).json(success({ user }));
        } catch (err) {
            logger.error(
                `Failed to get ${req.user.username}'s own data:`,
                err.msg ?? err
            );

            const statusCode =
                err.msg === errors.user_not_found
                    ? StatusCodes.NOT_FOUND
                    : StatusCodes.INTERNAL_SERVER_ERROR;

            res.status(statusCode).json(resErrors([err]));
        }
    });
};

export default me;
