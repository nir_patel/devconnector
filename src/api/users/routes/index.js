import register from './register.js';
import me from './me.js'
import login from './login.js';

export default { register, me, login };
