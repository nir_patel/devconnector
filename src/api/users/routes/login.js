import validations from '../validations/loginChecks.js';
import validateRequest from '../../../middlewares/validateRequest.js';
import signIn from '../../../services/signIn.js';
import { resErrors, success } from '../../../utils/parseMessage.js';
import { StatusCodes } from 'http-status-codes';
import signToken from '../../../utils/signToken.js';

// @route   GET api/users/login
// @desc    Authenticate user with username&password and get token
// @access  Public
const login = router => {
    router.post('/login', validations, validateRequest, async (req, res) => {
        const { username, password } = req.body;

        const result = await signIn(username, password);

        if (result.error) {
            return res.status(StatusCodes.UNAUTHORIZED).json(resErrors([result.error]));
        }

        const payload = extractData(result);

        signToken(payload, (err, token) => {
            if (err) {
                return res
                    .status(StatusCodes.INTERNAL_SERVER_ERROR)
                    .json(resErrors([err]));
            }
            return res.status(StatusCodes.OK).json(success({ token, ...payload }));
        });
    });
};

const extractData = ({ _id, username }) => ({ _id, username });

export default login;
