import auth from '../../../middlewares/auth.js';
import { StatusCodes } from 'http-status-codes';
import logger from '../../../logger/logger.js';
import deleteExperienceFromProfile from '../../../services/deleteExperienceFromProfile.js';
import { resErrors, success } from '../../../utils/parseMessage.js';

// @route   DELETE api/profiles/experience/:exp_id
// @desc    Delete experience from profile
// @access  Private
const deleteExperience = router => {
    router.delete('/experience/:exp_id', auth, async (req, res) => {
        const { _id } = req.user;
        const { exp_id } = req.params;

        const profile = await deleteExperienceFromProfile(_id, exp_id);

        if (profile.error) {
            logger.error(
                `Error deleting experience ${exp_id} from profile of user ${_id}:`,
                profile.error
            );

            return res.status(StatusCodes.BAD_REQUEST).json(resErrors([profile.error]));
        }

        res.status(StatusCodes.OK).json(success({ profile }));
    });
};

export default deleteExperience;
