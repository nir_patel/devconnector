import { StatusCodes } from 'http-status-codes';
import logger from '../../../logger/logger.js';
import auth from '../../../middlewares/auth.js';
import getProfileByUserId from '../../../services/getProfileByUserId.js';
import { resErrors, success } from '../../../utils/parseMessage.js';

// @route   GET api/profiles/me
// @desc    Get user's own profile
// @access  Private
const me = router => {
    router.get('/me', auth, async (req, res) => {
        try {
            const profile = await getProfileByUserId(req.user._id);

            res.status(StatusCodes.OK).json(success({ profile }));
        } catch (err) {
            logger.error(`Failed to get ${req.user.username}'s own profile data:`, err.msg);

            res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(resErrors([err]));
        }
    });
};

export default me;