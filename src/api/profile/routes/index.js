import me from './me.js';
import upsert from './upsert.js';
import getAll from './getAll.js';
import getOne from './getOne.js';
import deleteProfile from './delete.js';
import addExperience from './addExperience.js';
import deleteExperience from './deleteExperience.js';
import addEducation from './addEducation.js';
import deleteEducation from './deleteEducation.js';
import getGithubRepos from './getGithubRepos.js';
import getGitlabRepos from './getGitlabRepos.js';

export default {
    me,
    upsert,
    getAll,
    getOne,
    deleteProfile,
    addExperience,
    deleteExperience,
    addEducation,
    deleteEducation,
    getGithubRepos,
    getGitlabRepos
};
