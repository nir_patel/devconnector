import { StatusCodes } from 'http-status-codes';
import getGithubProjects from '../../../services/getGithubProjects.js';
import { resErrors, success } from '../../../utils/parseMessage.js';

// @route   GET api/profiles/github/:github_username
// @desc    Get user repos from Github
// @access  Public
const getGithubRepos = router =>
    router.get('/github/:github_username', async (req, res) => {
        const repos = await getGithubProjects(req.params.github_username);

        if (repos.error) {
            return res.status(StatusCodes.NOT_FOUND).json(resErrors([repos.error]));
        }

        res.status(StatusCodes.OK).json(success({ repos }));
    });

export default getGithubRepos;
