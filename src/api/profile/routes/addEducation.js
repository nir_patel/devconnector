import { StatusCodes } from 'http-status-codes';
import auth from '../../../middlewares/auth.js';
import validateRequest from '../../../middlewares/validateRequest.js';
import addEducationToUser from '../../../services/addEducationToUser.js';
import { resErrors, success } from '../../../utils/parseMessage.js';
import validations from '../validations/addEducationChecks.js';

// @route   PUT api/profiles/education
// @desc    Add profile education
// @access  Private
const addEducation = router => {
    router.put('/education', auth, validations, validateRequest, async (req, res) => {
        const profile = await addEducationToUser(req.user._id, req.body);

        if (profile.error) {
            return res
                .status(StatusCodes.INTERNAL_SERVER_ERROR)
                .json(resErrors([profile.error]));
        }

        res.status(StatusCodes.OK).json(success({ profile }));
    });
};

export default addEducation;
