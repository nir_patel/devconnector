import { StatusCodes } from 'http-status-codes';
import auth from '../../../middlewares/auth.js';
import validateRequest from '../../../middlewares/validateRequest.js';
import addExperienceToUser from '../../../services/addExperienceToUser.js';
import { resErrors, success } from '../../../utils/parseMessage.js';
import validations from '../validations/addExperienceChecks.js';

// @route   PUT api/profiles/experience
// @desc    Add profile experience
// @access  Private
const addExperience = router => {
    router.put('/experience', auth, validations, validateRequest, async (req, res) => {
        const profile = await addExperienceToUser(req.user._id, req.body);

        if (profile.error) {
            return res
                .status(StatusCodes.INTERNAL_SERVER_ERROR)
                .json(resErrors([profile.error]));
        }

        res.status(StatusCodes.OK).json(success({ profile }));
    });
};

export default addExperience;
