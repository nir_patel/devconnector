import { StatusCodes } from 'http-status-codes';
import getGitlabProjects from '../../../services/getGitLabProjects.js';
import { resErrors, success } from '../../../utils/parseMessage.js';

// @route   GET api/profiles/gitlab/:gitlab_username
// @desc    Get user repos from Gitlab
// @access  Public
const getGitlabRepos = router =>
    router.get('/gitlab/:gitlab_username', async (req, res) => {
        const repos = await getGitlabProjects(req.params.gitlab_username);

        if (repos.error) {
            return res.status(StatusCodes.NOT_FOUND).json(resErrors([repos.error]));
        }

        res.status(StatusCodes.OK).json(success({ repos }));
    });

export default getGitlabRepos;
