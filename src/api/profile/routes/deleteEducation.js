import auth from '../../../middlewares/auth.js';
import { StatusCodes } from 'http-status-codes';
import logger from '../../../logger/logger.js';
import deleteEducationFromProfile from '../../../services/deleteEducationFromProfile.js';
import { resErrors, success } from '../../../utils/parseMessage.js';

// @route   DELETE api/profiles/education/:edu_id
// @desc    Delete education from profile
// @access  Private
const deleteEducation = router => {
    router.delete('/education/:edu_id', auth, async (req, res) => {
        const { _id } = req.user;
        const { edu_id } = req.params;

        const profile = await deleteEducationFromProfile(_id, edu_id);

        if (profile.error) {
            logger.error(
                `Error deleting education ${edu_id} from profile of user ${_id}:`,
                profile.error
            );

            return res.status(StatusCodes.BAD_REQUEST).json(resErrors([profile.error]));
        }

        res.status(StatusCodes.OK).json(success({ profile }));
    });
};

export default deleteEducation;
