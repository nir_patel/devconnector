import { StatusCodes } from 'http-status-codes';
import logger from '../../../logger/logger.js';
import auth from '../../../middlewares/auth.js';
import deleteUser from '../../../services/deleteUser.js';
import { success } from '../../../utils/parseMessage.js';

// @route   DELETE api/profiles/delete
// @desc    Delete own profile, user & posts
// @access  Private
const deleteProfile = router => {
    router.delete('/delete', auth, async (req, res) => {
        const { _id } = req.user;

        await deleteUser(_id);

        logger.info(`User ${_id} deleted successfully`);

        res.status(StatusCodes.OK).json(success({ _id, msg: 'User deleted' }));
    });
};

export default deleteProfile;
