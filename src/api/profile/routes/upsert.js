import { StatusCodes } from 'http-status-codes';
import logger from '../../../logger/logger.js';
import auth from '../../../middlewares/auth.js';
import validateRequest from '../../../middlewares/validateRequest.js';
import upsertProfile from '../../../services/upsertProfile.js';
import validations from '../validations/upsertChecks.js';

// @route   POST api/profiles/upsert
// @desc    Create or update own user's profile
// @access  Private
const upsert = router => {
    router.post('/upsert', auth, validations, validateRequest, async (req, res) => {
        const { skills } = req.body;

        const profile = {
            ...req.body,
            skills: skills.split(',').map(skill => skill.trim())
        };

        const upsertResult = await upsertProfile(req.user._id, profile);

        logger.info('Successfully upserted profile for user', req.user._id);

        res.status(StatusCodes.OK).json(upsertResult);
    });
};

export default upsert;
