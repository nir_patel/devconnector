import { StatusCodes } from 'http-status-codes';
import logger from '../../../logger/logger.js';
import validations from '../validations/getOneChecks.js';
import getProfileByUserId from '../../../services/getProfileByUserId.js';
import { resErrors, success } from '../../../utils/parseMessage.js';
import validateRequest from '../../../middlewares/validateRequest.js';
import { errors } from '../../../../config/errors.js';

// @route   GET api/profiles/user/:userId
// @desc    Get profile by user id
// @access  Public
const getOne = router => {
    router.get('/user/:userId', validations, validateRequest, async (req, res) => {
        const { userId } = req.params;

        try {
            const profile = await getProfileByUserId(userId);

            res.status(StatusCodes.OK).json(success({ profile }));
        } catch (err) {
            logger.error(
                `Failed to get profile data from userId ${userId}:`,
                err.msg ?? err
            );

            res.status(
                err.msg === errors.profile_not_found
                    ? StatusCodes.NOT_FOUND
                    : StatusCodes.INTERNAL_SERVER_ERROR
            ).json(resErrors([err]));
        }
    });
};

export default getOne;
