import { StatusCodes } from 'http-status-codes';
import getAllProfiles from '../../../services/getAllProfiles.js';
import { success } from '../../../utils/parseMessage.js';

// @route   GET api/profiles
// @desc    Get all profiles
// @access  Public
const getAll = router => {
    router.get('/', async (req, res) => {
        const profiles = await getAllProfiles();

        res.status(StatusCodes.OK).json(success({ profiles }));
    });
};

export default getAll;
