import { param } from 'express-validator';

const validations = [param('github_username', 'Github username is required').notEmpty()];

export default validations;
