import { body } from 'express-validator';

const validations = [
    body('school', 'School is required').notEmpty(),
    body('degree', 'Degree is required').notEmpty(),
    body('fieldOfStudy', 'Field of study is required').notEmpty(),
    body('from', 'From date must be timestamp').isNumeric(),
    body('to', 'To date must be timestamp').optional().isNumeric()
];

export default validations;
