import { body } from 'express-validator';

const validations = [
    body('status', 'status is requried').notEmpty(),
    body('skills', 'skills are required').notEmpty(),
    body('skills', 'skills must be string').isString()
];

export default validations;
