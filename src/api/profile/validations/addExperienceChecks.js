import { body } from 'express-validator';

const validations = [
    body('title', 'Title is required').notEmpty(),
    body('company', 'Company is required').notEmpty(),
    body('from', 'From date must be timestamp').isNumeric(),
    body('to', 'To date must be timestamp').optional().isNumeric()
];

export default validations;
