import { param } from 'express-validator';

const validations = [param('userId', 'userId is required').notEmpty()];

export default validations;
