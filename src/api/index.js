import userRoutes from './users/routes/index.js';
import profilesRoutes from './profile/routes/index.js';
import postRoutes from './posts/routes/index.js';

export const users = userRoutes;
export const profiles = profilesRoutes;
export const posts = postRoutes;
