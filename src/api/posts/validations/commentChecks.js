import { body } from 'express-validator';

const validations = [body('text', 'Text is required').notEmpty()];

export default validations;
