import create from './create.js';
import getAll from './getAll.js';
import getById from './getById.js';
import deletePost from './delete.js';
import like from './like.js';
import unlike from './unlike.js';
import comment from './comment.js';
import deleteComment from './deleteComment.js';

export default {
    create,
    getAll,
    getById,
    deletePost,
    like,
    unlike,
    comment,
    deleteComment
};
