import { StatusCodes } from 'http-status-codes';
import logger from '../../../logger/logger.js';
import auth from '../../../middlewares/auth.js';
import getAllPosts from '../../../services/getAllPosts.js';
import { success } from '../../../utils/parseMessage.js';

// @route   GET api/posts
// @desc    Get all posts
// @access  Private
const getAll = router => {
    router.get('/', auth, async (req, res) => {
        const posts = await getAllPosts();

        logger.info('Fetched all posts');

        res.status(StatusCodes.OK).json(success({ posts }));
    });
};

export default getAll;
