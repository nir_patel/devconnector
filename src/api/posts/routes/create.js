import { StatusCodes } from 'http-status-codes';
import { errors } from '../../../../config/errors.js';
import auth from '../../../middlewares/auth.js';
import validateRequest from '../../../middlewares/validateRequest.js';
import getUserById from '../../../services/getUserById.js';
import uploadPost from '../../../services/uploadPost.js';
import { success, resErrors } from '../../../utils/parseMessage.js';
import validations from '../validations/createChecks.js';

// @route   POST api/posts
// @desc    Create a post
// @access  Private
const create = router => {
    router.post('/', auth, validations, validateRequest, async (req, res) => {
        let user;

        try {
            user = await getUserById(req.user._id);
        } catch (err) {
            logger.error(`Failed to get ${req.user.username}'s data:`, err.msg ?? err);

            const statusCode =
                err.msg === errors.user_not_found
                    ? StatusCodes.NOT_FOUND
                    : StatusCodes.INTERNAL_SERVER_ERROR;

            return res.status(statusCode).json(resErrors([err]));
        }

        const post = await uploadPost(req.user._id, user, req.body.text);

        res.status(StatusCodes.OK).json(success({ post }));
    });
};

export default create;
