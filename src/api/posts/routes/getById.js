import { StatusCodes } from 'http-status-codes';
import { errors } from '../../../../config/errors.js';
import logger from '../../../logger/logger.js';
import auth from '../../../middlewares/auth.js';
import getPostById from '../../../services/getPostById.js';
import { resErrors, success } from '../../../utils/parseMessage.js';

// @route   GET api/posts/:id
// @desc    Get post by id
// @access  Private
const getById = router => {
    router.get('/:id', auth, async (req, res) => {
        try {
            const post = await getPostById(req.params.id);

            res.status(StatusCodes.OK).json(success({ post }));
        } catch (err) {
            logger.error(`Failed to get post data of ${req.params.id}:`, err.msg ?? err);

            const statusCode =
                err.msg === errors.post_not_found
                    ? StatusCodes.NOT_FOUND
                    : StatusCodes.INTERNAL_SERVER_ERROR;

            res.status(statusCode).json(resErrors([err]));
        }
    });
};

export default getById;
