import { StatusCodes } from 'http-status-codes';
import auth from '../../../middlewares/auth.js';
import validateRequest from '../../../middlewares/validateRequest.js';
import addComment from '../../../services/addComment.js';
import { success } from '../../../utils/parseMessage.js';
import validations from '../validations/commentChecks.js';

// @route   POST api/posts/comment/:id
// @desc    Comment on a post
// @access  Private
const comment = router => {
    router.post('/comment/:id', auth, validations, validateRequest, async (req, res) => {
        const { params, user, body } = req;

        const { comments } = await addComment(params.id, user._id, body.text);

        res.status(StatusCodes.OK).json(success({ comments }));
    });
};

export default comment;
