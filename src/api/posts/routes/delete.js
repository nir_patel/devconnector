import { StatusCodes } from 'http-status-codes';
import { errors } from '../../../../config/errors.js';
import logger from '../../../logger/logger.js';
import auth from '../../../middlewares/auth.js';
import getPostById from '../../../services/getPostById.js';
import deletePost from '../../../services/deletePost.js';
import { resErrors, success } from '../../../utils/parseMessage.js';

// @route   DELETE api/posts/:id
// @desc    Delete a post
// @access  Private
const deletePostRoute = router => {
    router.delete('/:id', auth, async (req, res) => {
        const { id: postId } = req.params;
        const { _id: userId } = req.user;

        let post;
        try {
            post = await getPostById(postId);
        } catch (err) {
            logger.error(`Failed to find post ${postId}:`, err.msg ?? err);

            const statusCode =
                err.msg === errors.post_not_found
                    ? StatusCodes.NOT_FOUND
                    : StatusCodes.INTERNAL_SERVER_ERROR;

            return res.status(statusCode).json(resErrors([err]));
        }

        if (userId !== post.user.toString()) {
            return res
                .status(StatusCodes.UNAUTHORIZED)
                .json(resErrors([errors.user_unauthorized]));
        }

        await deletePost(postId);

        logger.info(`Post ${postId} deleted successfully`);

        res.status(StatusCodes.OK).json(success({ _id: postId, msg: 'Post deleted' }));
    });
};

export default deletePostRoute;
