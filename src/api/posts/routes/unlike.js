import { StatusCodes } from 'http-status-codes';
import auth from '../../../middlewares/auth.js';
import removeLikeFromPost from '../../../services/removeLikeFromPost.js';
import { success } from '../../../utils/parseMessage.js';

// @route   PUT api/posts/unlike/:id
// @desc    Unlike a post
// @access  Private
const unlike = router => {
    router.put('/unlike/:id', auth, async (req, res) => {
        const { likes } = await removeLikeFromPost(req.params.id, req.user._id);

        res.status(StatusCodes.OK).json(success({ likes }));
    });
};

export default unlike;
