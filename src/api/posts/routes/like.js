import { StatusCodes } from 'http-status-codes';
import auth from '../../../middlewares/auth.js';
import addLikeToPost from '../../../services/addLikeToPost.js';
import { success } from '../../../utils/parseMessage.js';

// @route   PUT api/posts/like/:id
// @desc    Like a post
// @access  Private
const like = router => {
    router.put('/like/:id', auth, async (req, res) => {
        const { likes } = await addLikeToPost(req.params.id, req.user._id);

        res.status(StatusCodes.OK).json(success({ likes }));
    });
};

export default like;
