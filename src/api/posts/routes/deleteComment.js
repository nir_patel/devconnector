import { StatusCodes } from 'http-status-codes';
import { errors } from '../../../../config/errors.js';
import auth from '../../../middlewares/auth.js';
import removeComment from '../../../services/removeComment.js';
import { resErrors, success } from '../../../utils/parseMessage.js';

const errorToStatusCode = {
    [errors.comment_not_found]: StatusCodes.NOT_FOUND,
    [errors.user_unauthorized]: StatusCodes.FORBIDDEN
};

// @route   DELETE api/posts/comment/:post_id/:comment_id
// @desc    Remove a comment from a post
// @access  Private
const deleteComment = router => {
    router.delete('/comment/:post_id/:comment_id', auth, async (req, res) => {
        const { post_id: postId, comment_id: commentId } = req.params;

        try {
            const { comments } = await removeComment(postId, req.user._id, commentId);

            res.status(StatusCodes.OK).json(success({ comments }));
        } catch (err) {
            const statusCode =
                errorToStatusCode[err.error] ?? StatusCodes.INTERNAL_SERVER_ERROR;

            res.status(statusCode).json(resErrors([err.error]));
        }
    });
};

export default deleteComment;
