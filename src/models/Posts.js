import mongoose from 'mongoose';
import PostSchema from '../entities/Post.js';

const PostModel = mongoose.model('post', new mongoose.Schema(PostSchema));

const Posts = {};

Posts.create = async post => {
    const postItem = new PostModel(post);

    return await postItem.save();
};

Posts.find = async (sort = { date: -1 }) => {
    return await PostModel.find().sort(sort);
};

Posts.findOneById = async _id => {
    return await PostModel.findOne({ _id });
};

Posts.removeOneById = async _id => {
    return await PostModel.findOneAndRemove({ _id });
};

export default Posts;
