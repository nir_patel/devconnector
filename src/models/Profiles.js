import mongoose from 'mongoose';
import ProfileSchema from '../entities/Profile.js';

const ProfileModel = mongoose.model('profile', new mongoose.Schema(ProfileSchema));

const Profiles = {};

Profiles.findOneByUserIdJoinUser = async (id, userFields = []) => {
    return await ProfileModel.findOne({ user: id }).populate('user', userFields);
};

Profiles.findOneByUserId = async userId => {
    return await ProfileModel.findOne({ user: userId });
};

Profiles.upsertOneByUserId = async (id, update = {}) => {
    return await ProfileModel.updateOne({ user: id }, { $set: update }, { upsert: true });
};

Profiles.save = async profile => {
    return await profile.save();
};

Profiles.getFields = () => Object.keys(ProfileSchema);

Profiles.findAll = async () => {
    return await ProfileModel.find({});
};

Profiles.removeOneByUserId = async id => {
    return await ProfileModel.findOneAndRemove({ user: id });
};

export default Profiles;
