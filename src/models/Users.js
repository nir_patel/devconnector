import mongoose from 'mongoose';
import UserSchema from '../entities/User.js';

const UserModel = mongoose.model('user', new mongoose.Schema(UserSchema));

const Users = {};

Users.findOneForLogin = async (query = {}, options = {}) => {
    return await UserModel.findOne(query, options).select('+password');
};

Users.findOneById = async _id => {
    return await UserModel.findOne({ _id });
};

Users.findOneByUsername = async username => {
    return await UserModel.findOne({ username });
};

Users.findOneByEmail = async email => {
    return await UserModel.findOne({ email });
};

Users.create = async user => {
    const userItem = new UserModel(user);

    return await userItem.save();
};

Users.removeOneById = async _id => {
    return await UserModel.findOneAndRemove({ _id });
};

export default Users;
