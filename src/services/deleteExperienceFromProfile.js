import logger from '../logger/logger.js';
import Profiles from '../models/Profiles.js';
import { innerError } from '../utils/parseMessage.js';

const deleteExperienceFromProfile = async (userId, experienceId) => {
    const profile = await Profiles.findOneByUserId(userId);

    if (!profile) {
        return innerError('profile_not_found');
    }

    profile.experience = profile.experience.filter(exp => exp.id !== experienceId);

    await Profiles.save(profile);

    logger.info(`Experience ${experienceId} deleted from profile of user ${userId}`);

    return profile;
};

export default deleteExperienceFromProfile;
