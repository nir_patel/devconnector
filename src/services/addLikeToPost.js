import logger from '../logger/logger.js';
import Posts from '../models/Posts.js';

const addLikeToPost = async (postId, userId) => {
    const post = await Posts.findOneById(postId);

    if (!post.likes.some(like => like.user.toString() === userId)) {
        post.likes.unshift({ user: userId });
        await post.save();
        logger.info(`Added like to ${postId} by ${userId}`);
    }

    return post;
};

export default addLikeToPost;
