import logger from '../logger/logger.js';
import Posts from '../models/Posts.js';

const uploadPost = async (userId, user, text) => {
    const { name, avatar, username } = user;

    const post = { text, name, avatar, user: userId };

    logger.info(`Uploading post by ${username}: ${text}`);

    const result = await Posts.create(post);

    logger.info(`Post by ${username} uploaded successfully`);

    return result;
};

export default uploadPost;
