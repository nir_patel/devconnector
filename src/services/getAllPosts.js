import logger from '../logger/logger.js';
import Posts from '../models/Posts.js';

const getAllPosts = async () => {
    logger.info('Fetching all posts...');
    return await Posts.find();
};

export default getAllPosts;
