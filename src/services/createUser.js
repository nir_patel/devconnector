import Users from '../models/Users.js';
import { innerError } from '../utils/parseMessage.js';
import { getAvatar } from '../utils/getAvatar.js';
import { encrypt } from '../utils/encrypt.js';
import logger from '../logger/logger.js';

const createUser = async userData => {
    const { username, name, email } = userData;

    const usernameExists = await Users.findOneByUsername(username);

    if (usernameExists) {
        return innerError('username_not_available');
    }

    const emailExists = await Users.findOneByEmail(email);

    if (emailExists) {
        return innerError('email_not_available');
    }

    const avatar = await getAvatar(email);

    const password = await encrypt(userData.password);

    logger.log(`Creating user ${username}...`);

    return await Users.create({ username, name, email, password, avatar });
};

export default createUser;
