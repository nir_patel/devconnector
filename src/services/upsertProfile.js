import Profiles from '../models/Profiles.js';
import supportedSocialMedias from '../utils/supportedSocialMedias.js';

const upsertProfile = async (userId, profile) => {
    const profileFields = Profiles.getFields();

    const update = {};

    profileFields.forEach(field => {
        if (field in profile) {
            update[field] = profile[field];
        }
    });

    update.social = {};

    supportedSocialMedias.forEach(media => {
        if (profile[media]) {
            update.social[media] = profile[media];
        }
    });

    const { upserted } = await Profiles.upsertOneByUserId(userId, update);

    return { ...update, existed: !upserted };
};

export default upsertProfile;
