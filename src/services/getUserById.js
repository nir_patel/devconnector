import { errors } from '../../config/errors.js';
import logger from '../logger/logger.js';
import Users from '../models/Users.js';

const getUserById = async id => {
    const user = await Users.findOneById(id);

    if (!user) {
        throw { msg: errors.user_not_found, userId: id };
    }

    logger.info(`Got ${user.username} full data:`, user);

    return user;
};

export default getUserById;
