import logger from '../logger/logger.js';
import Posts from '../models/Posts.js';
import { innerError } from '../utils/parseMessage.js';

const removeComment = async (postId, userId, commentId) => {
    const post = await Posts.findOneById(postId);

    const comment = post.comments.find(com => com.id === commentId);

    if (!comment) {
        throw innerError('comment_not_found');
    }

    if (userId !== comment.user.toString()) {
        throw innerError('user_unauthorized');
    }

    post.comments = post.comments.filter(com => com.id !== commentId);

    await post.save();

    logger.info(`${userId} removed their comment ${commentId} on ${postId}`);

    return post;
};

export default removeComment;
