import ProfileSchema from '../entities/Profile.js';
import logger from '../logger/logger.js';
import Profiles from '../models/Profiles.js';
import { innerError } from '../utils/parseMessage.js';

const addEducationToUser = async (userId, education) => {
    try {
        const profile = await Profiles.findOneByUserId(userId);

        logger.info(`Adding education from ${education.school} for profile`, profile);

        const newEducation = Object.keys(ProfileSchema.education[0]).reduce(
            (total, field) => ({ ...total, [field]: education[field] }),
            {}
        );

        profile.education.unshift(newEducation);

        await profile.save();

        logger.info(`Added education from ${education.school} for user`, userId);

        return profile;
    } catch (err) {
        logger.error('Error adding education to profile:', err);
        return innerError('generic_error');
    }
};

export default addEducationToUser;
