import axios from 'axios';
import config from '../../config/config.js';
import logger from '../logger/logger.js';
import { innerError } from '../utils/parseMessage.js';

const getGithubProjects = async githubUsername => {
    const uri = `${config.get('GITHUB_URL')}/${githubUsername}/repos`;

    try {
        const { data } = await axios.get(uri, {
            params: {
                per_page: 5,
                sort: 'created:asc',
                client_id: config.get('GITHUB_CLIENTID'),
                client_secret: config.get('GITHUB_SECRET')
            },
            headers: {
                'user-agent': 'node.js'
            }
        });

        logger.info(`Received Github projects for ${githubUsername}:`, data);

        return data;
    } catch (err) {
        logger.error('Error getting github projects:', err);
        return innerError('github_repo_not_found');
    }
};

export default getGithubProjects;
