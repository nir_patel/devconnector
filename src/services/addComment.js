import logger from '../logger/logger.js';
import Posts from '../models/Posts.js';
import Users from '../models/Users.js';

const addComment = async (postId, userId, text) => {
    const post = await Posts.findOneById(postId);
    const user = await Users.findOneById(userId);

    const { name, avatar } = user;

    const comment = {
        text,
        name,
        avatar,
        user: userId
    };

    post.comments.unshift(comment);

    await post.save();

    logger.info(`${userId} commented on ${postId}:`, text);

    return post;
};

export default addComment;
