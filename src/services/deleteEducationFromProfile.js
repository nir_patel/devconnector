import logger from '../logger/logger.js';
import Profiles from '../models/Profiles.js';
import { innerError } from '../utils/parseMessage.js';

const deleteEducationFromProfile = async (userId, educationId) => {
    const profile = await Profiles.findOneByUserId(userId);

    if (!profile) {
        return innerError('profile_not_found');
    }

    profile.education = profile.education.filter(
        education => education.id !== educationId
    );

    await Profiles.save(profile);

    logger.info(`Education ${educationId} deleted from profile of user ${userId}`);

    return profile;
};

export default deleteEducationFromProfile;
