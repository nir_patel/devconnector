import Posts from '../models/Posts.js';

const deletePost = async postId => {
    return await Posts.removeOneById(postId);
};

export default deletePost;
