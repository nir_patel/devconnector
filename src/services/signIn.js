import logger from '../logger/logger.js';
import Users from '../models/Users.js';
import { compare } from '../utils/encrypt.js';
import { innerError } from '../utils/parseMessage.js';

const signIn = async (username, password) => {
    const user = await Users.findOneForLogin({ username });

    if (!user || !(await compare(password, user.password))) {
        logger.error(`Login failed for ${username}`);
        return innerError('login_failed');
    }

    logger.info(`Successful login for ${username}`);

    return user;
};

export default signIn;
