import { errors } from '../../config/errors.js';
import logger from '../logger/logger.js';
import Posts from '../models/Posts.js';

const getPostById = async id => {
    let post;

    try {
        post = await Posts.findOneById(id);
    } catch (err) {
        if (err.kind !== 'ObjectId') {
            throw { msg: err, postId: id };
        }
    }

    if (!post) {
        throw { msg: errors.post_not_found, postId: id };
    }

    logger.info(`Got full post data of ${id}`, post);

    return post;
};

export default getPostById;
