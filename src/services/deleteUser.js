import Profiles from '../models/Profiles.js';
import Users from '../models/Users.js';

const deleteUser = async userId => {
    // @TODO delete posts

    await Profiles.removeOneByUserId(userId);

    await Users.removeOneById(userId);
};

export default deleteUser;
