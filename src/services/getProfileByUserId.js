import { errors } from '../../config/errors.js';
import logger from '../logger/logger.js';
import Profiles from '../models/Profiles.js';

const getProfileByUserId = async id => {
    const userExtraFields = ['name', 'avatar'];

    let profile;

    try {
        profile = await Profiles.findOneByUserIdJoinUser(id, userExtraFields);
    } catch (err) {
        if (err.kind !== 'ObjectId') {
            throw { msg: err, userId: id };
        }
    }

    if (!profile) {
        throw { msg: errors.profile_not_found, userId: id };
    }

    logger.info(`Got full profile data:`, profile);

    return profile;
};

export default getProfileByUserId;
