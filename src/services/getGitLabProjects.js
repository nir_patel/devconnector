import axios from 'axios';
import config from '../../config/config.js';
import logger from '../logger/logger.js';
import { innerError } from '../utils/parseMessage.js';

const getGitlabProjects = async gitlabUsername => {
    const uri = `${config.get('GITLAB_URL')}/users/${gitlabUsername}/projects`;

    try {
        const { data } = await axios.get(uri, {
            params: {
                membership: true
            },
            headers: {
                'user-agent': 'node.js'
            }
        });

        logger.info(`Received Gitlab projects for ${gitlabUsername}:`, data);

        return data;
    } catch (err) {
        logger.error('Error getting gitlab projects:', err);
        return innerError('gitlab_repo_not_found');
    }
};

export default getGitlabProjects;
