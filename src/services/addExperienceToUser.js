import ProfileSchema from '../entities/Profile.js';
import logger from '../logger/logger.js';
import Profiles from '../models/Profiles.js';
import { innerError } from '../utils/parseMessage.js';

const addExperienceToUser = async (userId, experience) => {
    try {
        const profile = await Profiles.findOneByUserId(userId);

        logger.info(`Adding experience ${experience.title} for profile`, profile);

        const newExperience = Object.keys(ProfileSchema.experience[0]).reduce(
            (total, field) => ({ ...total, [field]: experience[field] }),
            {}
        );

        profile.experience.unshift(newExperience);

        await profile.save();

        logger.info(`Added experience ${experience.title} for user`, userId);

        return profile;
    } catch (err) {
        logger.error('Error adding experience to profile:', err);
        return innerError('generic_error');
    }
};

export default addExperienceToUser;
