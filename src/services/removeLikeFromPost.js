import logger from '../logger/logger.js';
import Posts from '../models/Posts.js';

const removeLikeFromPost = async (postId, userId) => {
    const post = await Posts.findOneById(postId);

    post.likes = post.likes.filter(like => like.user.toString() !== userId);

    await post.save();

    logger.info(`Removed like from ${postId} by ${userId}`);

    return post;
};

export default removeLikeFromPost;
