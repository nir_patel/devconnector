import Profiles from '../models/Profiles.js';

const getAllProfiles = async () => {
    return await Profiles.findAll();
};

export default getAllProfiles;
