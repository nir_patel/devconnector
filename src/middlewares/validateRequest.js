import { validationResult } from 'express-validator';
import { StatusCodes } from 'http-status-codes';
import { resErrors } from '../utils/parseMessage.js';

const validateRequest = (req, res, next) => {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
        return res
            .status(StatusCodes.BAD_REQUEST)
            .json(resErrors(validationErrors.array().map(err => err.msg)));
    }

    next();
};

export default validateRequest;
