import jwt from 'jsonwebtoken';
import config from '../../config/config.js';
import { StatusCodes } from 'http-status-codes';
import { errors } from '../../config/errors.js';
import { resErrors } from '../utils/parseMessage.js';

const auth = (req, res, next) => {
    const token = req.header('x-auth-token');

    try {
        const user = jwt.verify(token, config.get('JWT_SECRET'));

        req.user = user;

        next();
    } catch (err) {
        res.status(StatusCodes.UNAUTHORIZED).json(resErrors([errors.invalid_token]));
    }
};

export default auth;