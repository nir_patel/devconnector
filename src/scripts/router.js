import express from 'express';
import { users, profiles, posts } from '../api/index.js';

const declareEndpoints = app => {
    declareEndpointsForGroup(app, users, 'users');
    declareEndpointsForGroup(app, profiles, 'profiles');
    declareEndpointsForGroup(app, posts, 'posts');
};

const declareEndpointsForGroup = (app, group, key) => {
    const router = express.Router();
    Object.values(group).forEach(route => route(router));
    app.use(`/api/${key}`, router);
};

export default declareEndpoints;
