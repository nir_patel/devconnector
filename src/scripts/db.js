import mongoose from 'mongoose';
import logger from '../logger/logger.js';
import config from '../../config/config.js';

const { connect } = mongoose;

const dbURI = config.get('MONGO_URI');

export const connectDB = async () => {
    try {
        await connect(dbURI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        });

        logger.info('MongoDB Connected');
    } catch (err) {
        logger.error(`Error connecting to mongodb: ${err?.message}`);
        process.exit(1);
    }
};
