import colors from 'colors';

colors.enable();

colors.setTheme({
    log: 'white',
    info: 'blue',
    debug: 'green',
    warn: 'orange',
    error: 'red'
});

const log = (...args) => {
    console.log(colors.log(...args));
};

const info = (...args) => {
    console.info(colors.info(...args));
};

const debug = (...args) => {
    console.debug(colors.debug(...args));
};

const warn = (...args) => {
    console.warn(colors.warn(...args));
};

const error = (...args) => {
    console.error(colors.error(...args));
};

const logger = { log, info, debug, warn, error };

export default logger;
