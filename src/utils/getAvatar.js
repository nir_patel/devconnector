import gravatar from 'gravatar';

export const getAvatar = async (email) => {
    return gravatar.url(email, { s: '200', r: 'pg', d: 'mm' });
};
