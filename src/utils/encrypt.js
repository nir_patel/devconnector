import bcrypt from 'bcryptjs';

export const encrypt = async (password) => {
    const salt = await bcrypt.genSalt(10);

    const encrypted = await bcrypt.hash(password, salt);

    return encrypted;
};

export const compare = async (input, actual) => {
    return await bcrypt.compare(input, actual);
};
