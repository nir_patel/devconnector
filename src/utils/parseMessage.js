import { errors } from '../../config/errors.js';

export const innerError = name => {
    return { error: errors[name] };
};

export const success = body => {
    return {
        ...body,
        success: true
    };
};

export const resErrors = errMessages => {
    return {
        success: false,
        errors: errMessages.map(msg => (typeof msg === 'object' ? msg : { msg }))
    };
};
