import jwt from 'jsonwebtoken';
import config from '../../config/config.js';

const signToken = (payload, callback) => {
    jwt.sign(
        payload,
        config.get('JWT_SECRET'),
        { expiresIn: config.get('TOKEN_EXPIRATION') * 1000 },
        callback
    );
};

export default signToken;
