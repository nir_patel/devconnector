import express from 'express';
import { connectDB } from './scripts/db.js';
import logger from './logger/logger.js';
import config from '../config/config.js';
import declareAPI from './scripts/router.js';

const app = express();

await connectDB();

app.use(express.json({ extended: false }));

app.get('/', (_, res) => {
    res.send('API Running');
});

declareAPI(app);

const PORT = config.get('PORT') || 5000;

app.listen(PORT, () => {
    logger.log(`Server started on port ${PORT}`);
});
